import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ContatoService } from "../shared/services/contato.service";

import Contato from "../shared/models/contato.model";

@Component({
  selector: "app-agenda-contato",
  templateUrl: "./agenda-contato.component.html",
  styleUrls: ["./agenda-contato.component.scss"]
})
export class AgendaContatoComponent implements OnInit {
  public contatos: Contato[];

  constructor(private contatoService: ContatoService, private router: Router) {}

  async ngOnInit() {
    this.contatos = await this.contatoService.getContato();
  }

  navegarDetalhes(contato: Contato) {
    localStorage.setItem("contatoSelecionado", JSON.stringify(contato));
    this.router.navigate([`/empresa`]);
  }
}
