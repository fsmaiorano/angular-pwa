import { Component, OnInit } from "@angular/core";
import {
  Router,
  ActivatedRoute,
  ActivatedRouteSnapshot,
  RouterEvent,
  UrlSegment
} from "@angular/router";
import Contato from "src/app/shared/models/contato.model";

@Component({
  selector: "app-detalhe-contato",
  templateUrl: "./detalhe-contato.component.html",
  styleUrls: ["./detalhe-contato.component.scss"]
})
export class DetalheContatoComponent implements OnInit {
  public contato: Contato;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.activatedRoute.url.subscribe((url: UrlSegment[]) => {
      if (url[0].path === "empresa") {
        const contato = localStorage.getItem("contatoSelecionado");
        if (!contato) return this.router.navigate(["/"]);

        this.contato = JSON.parse(contato);
      }
    });
  }

  iniciarChamada() {
    this.router.navigate(["/chamada"]);
  }
}
