import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaContatoComponent } from './agenda-contato.component';

describe('AgendaContatoComponent', () => {
  let component: AgendaContatoComponent;
  let fixture: ComponentFixture<AgendaContatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendaContatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaContatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
