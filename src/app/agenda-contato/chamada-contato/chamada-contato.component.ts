import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, UrlSegment } from "@angular/router";
import Contato from "src/app/shared/models/contato.model";

@Component({
  selector: "app-chamada-contato",
  templateUrl: "./chamada-contato.component.html",
  styleUrls: ["./chamada-contato.component.scss"]
})
export class ChamadaContatoComponent implements OnInit {
  public contato: Contato;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.activatedRoute.url.subscribe((url: UrlSegment[]) => {
      if (url[0].path === "chamada") {
        const contato = localStorage.getItem("contatoSelecionado");
        if (!contato) return this.router.navigate(["/"]);

        this.contato = JSON.parse(contato);
      }
    });
  }

  encerrarChamada() {
    this.router.navigate(["/"]);
  }
}
