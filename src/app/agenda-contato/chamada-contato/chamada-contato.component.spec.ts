import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChamadaContatoComponent } from './chamada-contato.component';

describe('ChamadaContatoComponent', () => {
  let component: ChamadaContatoComponent;
  let fixture: ComponentFixture<ChamadaContatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChamadaContatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChamadaContatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
