import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HeaderComponent } from "./header/header.component";
import { ContatoService } from "./services/contato.service";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, HttpModule, HttpClientModule],
  exports: [HeaderComponent, HttpModule, HttpClientModule],
  providers: [ContatoService]
})
export class SharedModule {}
