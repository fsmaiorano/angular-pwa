export default interface Contato {
  nome: string;
  dataNascimento: Date;
  endereco: Endereco;
  telefones: Telefone[];
  ativo: boolean;
}

interface Telefone {
  numero: string;
  tipoTelefone: string;
  ativo: boolean;
}

interface Endereco {
  rua: string;
  numero: number;
  complemento: string;
  cep: string;
  bairro: string;
  cidade: string;
  uf: string;
}
