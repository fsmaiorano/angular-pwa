import { Injectable } from "@angular/core";
import { Response, Http } from "@angular/http";
import { environment } from "src/environments/environment";

import Contato from "../models/contato.model";

@Injectable()
export class ContatoService {
  constructor(private http: Http) {}

  public async getContato(): Promise<Contato[]> {
    try {
      const response = await this.http
        .get(`${environment.apiUrl}/5bdf4f3a310000ea149e4009`)
        .toPromise();
      const { contatos } = response.json();
      return this.extractData(contatos);
    } catch (error) {
      return this.handleError(error);
    }
  }

  private extractData(response: Response): any {
    return Promise.resolve(response);
  }
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
