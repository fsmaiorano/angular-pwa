import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { AgendaContatoComponent } from "./agenda-contato/agenda-contato.component";
import { SharedModule } from "./shared/shared.module";
import { DetalheContatoComponent } from "./agenda-contato/detalhe-contato/detalhe-contato.component";
import { ChamadaContatoComponent } from './agenda-contato/chamada-contato/chamada-contato.component';

@NgModule({
  declarations: [AppComponent, AgendaContatoComponent, DetalheContatoComponent, ChamadaContatoComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
