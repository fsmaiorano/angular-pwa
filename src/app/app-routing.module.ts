import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AgendaContatoComponent } from "./agenda-contato/agenda-contato.component";
import { DetalheContatoComponent } from "./agenda-contato/detalhe-contato/detalhe-contato.component";
import { ChamadaContatoComponent } from "./agenda-contato/chamada-contato/chamada-contato.component";

const routes: Routes = [
  {
    path: "",
    component: AgendaContatoComponent
  },
  {
    path: "empresa",
    component: DetalheContatoComponent
  },
  {
    path: "chamada",
    component: ChamadaContatoComponent
  },
  {
    path: "**",
    component: AgendaContatoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
